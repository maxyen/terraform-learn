provider "aws" {

}

variable avail_zone{}

variable "cidr_blocks" {
  description = "cidr block and name tags for vpc and subnets"
  type=list(object({
    cidr_block=string
    name=string
  }))
}

variable "environment" {
  description = "deployment environment"
}
resource "aws_vpc" "test-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block
  tags={
    Name:var.cidr_blocks[0].name
  
  }
}

resource "aws_subnet" "test-subnet-1" {
  vpc_id =aws_vpc.test-vpc.id
  cidr_block = var.cidr_blocks[1].cidr_block
  availability_zone = var.avail_zone
    tags={
    Name:var.cidr_blocks[1].name
  }
}




